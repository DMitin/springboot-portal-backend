package ru.dmitin.portal.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.dmitin.portal.domain.Employee;
import ru.dmitin.portal.domain.Skill;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@RequestMapping(value = "/hello")
public class HelloController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Skill hello() {
        return new Skill("Java");
    }
/*
    @RequestMapping("/isLogined")
    public void isLogined(final Principal principal, HttpServletResponse response) {
        if (principal == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
        }
    }
    */
}
