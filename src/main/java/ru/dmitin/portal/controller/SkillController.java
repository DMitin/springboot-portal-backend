/*
 * Copyright [2011-2016] "Neo Technology"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 *
 */
package ru.dmitin.portal.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.dmitin.portal.domain.Skill;
import ru.dmitin.portal.repository.SkillRepository;


@RestController
@RequestMapping(value = "/api/skill")
public class SkillController {

	private SkillRepository skillRepository;

	@Autowired
	public SkillController(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Skill> readAll() {
		Iterable<Skill> skills = skillRepository.findAll();
		for(Skill s : skills) {
			s.clearFirstLevel();
		}
		return skills;

	}

	@RequestMapping(method = RequestMethod.POST)
	public Skill create(@RequestBody Skill skill) {
		return skillRepository.save(skill);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Skill read(@PathVariable Long id) {
		return skillRepository.findById(id).get();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		Skill skill = skillRepository.findById(id).get();
		skillRepository.delete(skill);
	}


	@Transactional
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Skill update(@PathVariable Long id, @RequestBody Skill update) {
		final Skill existing = skillRepository.findById(id).get();
		existing.updateFrom(update);
		return skillRepository.save(existing);
	}
}
