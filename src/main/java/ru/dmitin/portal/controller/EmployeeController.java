/*
 * Copyright [2011-2016] "Neo Technology"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 *
 */
package ru.dmitin.portal.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.dmitin.portal.domain.Employee;
import ru.dmitin.portal.domain.Skill;
import ru.dmitin.portal.repository.EmployeeRepository;


@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeController {

	private EmployeeRepository subjectRepository;

	@Autowired
	public EmployeeController(EmployeeRepository subjectRepository) {
		this.subjectRepository = subjectRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Employee> readAll() {
		return subjectRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Employee create(@RequestBody Employee employee) {
		return subjectRepository.save(employee);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Employee read(@PathVariable Long id) {
		return subjectRepository.findById(id).get();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		Employee employee = subjectRepository.findById(id).get();
		subjectRepository.delete(employee);
	}

	@Transactional
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Employee update(@PathVariable Long id, @RequestBody Employee update) {
		final Employee existing = subjectRepository.findById(id).get();
		existing.updateFrom(update);
		return subjectRepository.save(existing);
	}

	@Transactional
	@RequestMapping(value = "/addKnowSkill/{id}", method = RequestMethod.PUT)
	public Employee update(@PathVariable Long id, @RequestBody Skill skill ) {
		final Employee existing = subjectRepository.findById(id).get();
		existing.getKnownSkills().add(skill);
		return subjectRepository.save(existing);
	}
}
