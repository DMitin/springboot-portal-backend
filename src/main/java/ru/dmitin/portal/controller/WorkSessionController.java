package ru.dmitin.portal.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.dmitin.portal.domain.WorkSession;
import ru.dmitin.portal.repository.WorkSessionRepository;

/**
 * Created by demi0416 on 28.06.2017.
 */

@RestController
@RequestMapping(value = "/api/workSession")
public class WorkSessionController {

    @Autowired
    WorkSessionRepository workSessionRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public WorkSession read(@PathVariable Long id) {
        return workSessionRepository.findById(id).get();
    }

    @RequestMapping(method = RequestMethod.POST)
    public WorkSession create(@RequestBody WorkSession employee) {
        return workSessionRepository.save(employee);
    }
}
