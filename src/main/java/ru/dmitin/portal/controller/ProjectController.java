package ru.dmitin.portal.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.dmitin.portal.domain.Project;
import ru.dmitin.portal.repository.ProjectRepository;

/**
 * Created by demi0416 on 28.06.2017.
 */

@RestController
@RequestMapping(value = "/api/project")
public class ProjectController {

    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Project read(@PathVariable Long id) {
        return projectRepository.findById(id).get();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Project create(@RequestBody Project employee) {
        return projectRepository.save(employee);
    }
}
