package ru.dmitin.portal.repository;


import org.springframework.data.repository.CrudRepository;
import ru.dmitin.portal.domain.WorkSession;

/**
 * Created by demi0416 on 28.06.2017.
 */
public interface WorkSessionRepository extends CrudRepository<WorkSession, Long> {
}
