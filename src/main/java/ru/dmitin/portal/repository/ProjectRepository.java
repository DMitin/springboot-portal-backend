package ru.dmitin.portal.repository;


import org.springframework.data.repository.CrudRepository;
import ru.dmitin.portal.domain.Project;

/**
 * Created by demi0416 on 28.06.2017.
 */
public interface ProjectRepository extends CrudRepository<Project, Long> {
}
