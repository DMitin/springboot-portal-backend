/**
 * 
 */
package ru.dmitin.portal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.dmitin.portal.repository.EmployeeRepository;

import java.util.Collection;


/**
 * REMOVE IT
 *
 */
@Component
public class CustomUserDetailsService implements UserDetailsService
{
	@Autowired
	EmployeeRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException
	{

		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode("pass1");
		return new org.springframework.security.core.userdetails.User(
                "user1",
				hashedPassword,
                getAuthorities()
                );

    }

	private static Collection<? extends GrantedAuthority> getAuthorities()
    {
        String[] userRoles = new String[] {"IT"};
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }

}
