package ru.dmitin.portal.domain;


import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by demi0416 on 27.06.2017.
 */

@NodeEntity
public class WorkSession {

    @GraphId
    private Long id;
    private String description;

    @Relationship(type = "HAS_SESSION", direction = Relationship.INCOMING)
    private Set<Employee> employees;

    @Relationship(type = "USE_SKILLS")
    private Set<Skill> skills;

    @Relationship(type = "IN_PROJECT")
    private Set<Project> projects;

    public WorkSession() {
        employees = new HashSet<>();
        skills = new HashSet<>();
        projects = new HashSet<>();
    }

    public void clearSecondLevel() {
        this.employees = new HashSet<>();
        this.skills = new HashSet<>();
        this.projects = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
