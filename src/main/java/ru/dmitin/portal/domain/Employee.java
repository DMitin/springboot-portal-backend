/*
 * Copyright [2011-2016] "Neo Technology"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 *
 */
package ru.dmitin.portal.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Employee {

	@GraphId
	private Long id;
	private String name;

	@Relationship(type = "KNOW")
	private Set<Skill> knownSkills;

	@Relationship(type = "WANT_KNOW")
	private Set<Skill> wantedSkills;

	@Relationship(type = "HAS_SESSION")
	private Set<WorkSession> sessions;

	public Employee(String name) {
		this();
		this.name = name;
	}

	public Employee() {
		this.knownSkills = new HashSet<>();
		this.wantedSkills = new HashSet<>();
		this.sessions = new HashSet<>();
	}

	public void clearSecondLevel() {
		this.knownSkills = new HashSet<>();
		this.wantedSkills = new HashSet<>();
		this.sessions = new HashSet<>();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Skill> getKnownSkills() {
		return knownSkills;
	}

	public Set<Skill> getWantedSkills() {
		return wantedSkills;
	}

	public Set<WorkSession> getSessions() {
		return sessions;
	}

	public void updateFrom(Employee employee) {
		this.name = employee.getName();
	}
}
