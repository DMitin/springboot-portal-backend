/*
 * Copyright [2011-2016] "Neo Technology"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 *
 */
package ru.dmitin.portal.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Skill {

	@GraphId
	private Long id;

	private String name;

	@Relationship(type = "KNOW", direction = Relationship.INCOMING)
	private Set<Employee> employeesKnow;

	@Relationship(type = "WANT_KNOW", direction = Relationship.INCOMING)
	private Set<Employee> employeesWantKnow;

	@Relationship(type = "USE_SKILLS", direction = Relationship.INCOMING)
	private Set<WorkSession> sessions;

	public Skill(String name) {
		this();
		this.name = name;
	}

	public Skill() {
		this.employeesKnow = new HashSet<>();
		this.employeesWantKnow = new HashSet<>();
		this.sessions = new HashSet<>();
	}

	public void clearFirstLevel() {
		for (Employee employee: employeesKnow) {
			employee.clearSecondLevel();
		}
		for (Employee employee: employeesWantKnow) {
			employee.clearSecondLevel();
		}
		for (WorkSession ws: sessions) {
			ws.clearSecondLevel();
		}
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Employee> getEmployeesKnow() {
		return employeesKnow;
	}

	public Set<Employee> getEmployeesWantKnow() {
		return employeesWantKnow;
	}

	public Set<WorkSession> getSessions() {
		return sessions;
	}

	public void updateFrom(Skill skill) {
		this.name = skill.name;
	}
}
