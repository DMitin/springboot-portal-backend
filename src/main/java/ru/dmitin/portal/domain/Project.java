package ru.dmitin.portal.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by demi0416 on 28.06.2017.
 */

@NodeEntity
public class Project {

    @GraphId
    private Long id;
    private String name;

    @Relationship(type = "IN_PROJECT", direction = Relationship.INCOMING)
    private Set<WorkSession> sessions;


    public Project() {
        this.sessions = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<WorkSession> getSessions() {
        return sessions;
    }

    public void setSessions(Set<WorkSession> sessions) {
        this.sessions = sessions;
    }
}
