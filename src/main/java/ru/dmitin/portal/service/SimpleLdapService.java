package ru.dmitin.portal.service;

import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

@Service
public class SimpleLdapService {

    private static final String LDAP_URL = "ldap://domain.corp:389";
    private static final String AUTH_MECHANISM = "simple";
    private static final String DOMAIN_PREFIX = "domain\\";

    public boolean isValid(String username, String password) {

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);


        env.put(Context.SECURITY_AUTHENTICATION, AUTH_MECHANISM);
        env.put(Context.SECURITY_PRINCIPAL, username.toLowerCase().startsWith(DOMAIN_PREFIX) ? username : DOMAIN_PREFIX.concat(username));
        env.put(Context.SECURITY_CREDENTIALS, password);

        // Create the initial context
        try {
            DirContext ctx = new InitialDirContext(env);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
